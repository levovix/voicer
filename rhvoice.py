import subprocess, uuid, pathlib, os

voices = [
  "Aleksandr+Alan",
  "Artemiy+Alan",
  "Anna+CLB",
]

def writeFile(file, text=""):
  fs = open(file, "w")
  fs.write(text)
  fs.close()

def deleteFile(file):
  os.remove(file)

def fileExists(file):
  return os.path.isfile(file)

def currentDir():
  return pathlib.Path().resolve()

def to_voice(text="Привет, мир!", voice=0, outputFormat="mp3"):
  if voice not in range(len(voices)):
    print(f"Неизвестный голос (№{voice})")
    return
  if outputFormat not in ["mp3", "wav", "ogg"]:
    print(f"Неизвестный формат аудио (№{outputFormat})")
    return

  uname = str(uuid.uuid4())
  writeFile(f"{uname}.txt", text)
  process = subprocess.Popen(["./rhvoice", "-o", f"{uname}.{outputFormat}", "-i", f"{uname}.txt", "-p", voices[voice]])
  process.wait()
  
  assert(process.returncode == 0)
  
  deleteFile(f"{uname}.txt")
  
  if not fileExists(f"{uname}.{outputFormat}"):
    return ""
  else:
    return f"{currentDir()}/{uname}.{outputFormat}"

if __name__ == "__main__":
  print(to_voice("Это синтезированная речь"))
