import json, random, functools, asyncio, signal
import telebot, rhvoice

token = "1822252092:AAGLTZA4WY5OhSUNyYmfhWbBcAhccA5LDiQ"
bot = telebot.TeleBot(token)

class ChatState:
  def __init__(self, chat_id):
    self.id = chat_id
    self.has_licence = False
    self.voice = 0
    self.text = True
    self.kanegdotes = True
    self.style = 0
  
  def toDict(self):
    return {"id": self.id, "has_licence": self.has_licence, "voice": self.voice, "text": self.text, "kanegdotes": self.kanegdotes, "style": self.style}

chats = {}
anegdotes = []
kanegdotes = {
  "ботик": ["Шёл по улице котик, сказал котик: 'я не ботик'"], # пример
  "котик": ["Купила кошку, оказалась взбалмошной и очень дурашливой, но милой. Слышу как-то шорох из кухни, думаю, опять на стол залезла. Прихожу, точно, это чучело печенье из пакета вытряхнуло, пакет на голову надело и ползет мимо меня как крокодил, сквозь пакет таращится.",
  "Днём, когда кот дрыхнет, я считаю своим долгом потеребить его, несмотря на всё его недовольное ворчание. С пяти до семи утра ситуация меняется с точностью до наоборот.Кто кому мстит?!",
  "- Чего кот орёт? - Он спрашивает, какого хрена… - Какого хрена - что? - В целом",
  "- Мурзик, давай скажем папе с мамой, что и конфеты ты все съел. Какая тебе разница, ведь пилюлей за поваленную ёлку тебе уже не избежать."],
  "кот": ["Купила кошку, оказалась взбалмошной и очень дурашливой, но милой. Слышу как-то шорох из кухни, думаю, опять на стол залезла. Прихожу, точно, это чучело печенье из пакета вытряхнуло, пакет на голову надело и ползет мимо меня как крокодил, сквозь пакет таращится.",
  "Днём, когда кот дрыхнет, я считаю своим долгом потеребить его, несмотря на всё его недовольное ворчание. С пяти до семи утра ситуация меняется с точностью до наоборот.Кто кому мстит?!",
  "- Чего кот орёт? - Он спрашивает, какого хрена… - Какого хрена - что? - В целом",
  "- Мурзик, давай скажем папе с мамой, что и конфеты ты все съел. Какая тебе разница, ведь пилюлей за поваленную ёлку тебе уже не избежать."],
  'собака' : ["Надпись на воротах дома: 'Собака не злая. Но любит смотреть, как я выбрасываю вас с крыльца!'.", "Взросление - это когда человек, гуляя с собакой, начинает убирать за ней какашки, а старость - это когда ещё и за чужими тоже.", 
  "Я сводил свою чихуахуа на конкурс 'самый уродливый кобель' и взял первое место! Чихуахуа взяла третье."],
  # "Алена" : ["Было у гопника три дочери. Одну он назвал Алена. Вторую - Тычена. Третью - Ващена..."],
  "огурец": ["Должен вам признаться, что в вопросах, касающихся солёных огурцов, я — консерватор.", "Есть какая-то высшая несправедливость в том, что человеку начинают нравиться соленые огурцы, как раз когда его рука перестает пролезать в банку"],
  "утро"  : ["'Как же хорошо, что я вырос, и мне не надо вставать в школу в 8 утра' — думал я, вставая на работу в 7 утра.", "Утро. Жена мужу: — Вася, вставай, на работу пора. Вставай. Ну, Вася! Нет, Вася, ты давай весь вставай..."]
}

def жалоба_на_отсутствие_лицензии(state):
  if state.style == 0: return "Для использования бота требуется приобрести лицензию, выполните /request_licence для того чтобы разработчики узнали, что вы хотите купить лицензию"
  elif state.style == 0: return "Слушай, так не пойдёт. Я не собираюсь работать за бесплатно. Давай-ка ты напишешь /request_licence и пойдёшь купишь лицензию"

def get_state(chat_id) -> ChatState:
  chat_id = str(chat_id)
  if chat_id not in chats.keys():
    chats[chat_id] = ChatState(chat_id)
  return chats[chat_id]

def save_chats():
  d = {}
  for key in chats.keys():
    d[key] = chats[key].toDict()
  with open("chats.json", "w") as fs:
    fs.write(json.dumps(d, indent=2))
    fs.close()

def load_chats():
  with open("chats.json", "r") as fs:
    d = json.load(fs)
    for key in d.keys():
      print(d[key])
      o = ChatState(key)
      try:
        o.id = key
        o.has_licence = d[key]["has_licence"]
        o.voice = d[key]["voice"]
        o.text = d[key]["text"]
        o.kanegdotes = d[key]["kanegdotes"]
      except: pass
      chats[key] = o
    fs.close()

@functools.cache
def to_voice(text: str, voice=0):
  ## Озвучивает текст
  audio_file = rhvoice.to_voice(text, voice)
  if audio_file == "": return
  
  audio = open(audio_file, 'rb')
  res = audio.read()
  audio.close()
  rhvoice.deleteFile(audio_file)

  return res

def send_voice(state, text):
  ## Озвучивает и отправляет текст
  if state.text:
    bot.send_voice(state.id, to_voice(text, state.voice), text)
  else:
    bot.send_voice(state.id, to_voice(text, state.voice))

def parse_message(message):
  return get_state(message.chat.id), message.text

def requires_licence(f):
  def wrapper(message, *args, **kwargs):
    state, text = parse_message(message)
    if state.has_licence:
      f(message, *args, **kwargs)
    else:
      send_voice(state, жалоба_на_отсутствие_лицензии(state))
  return wrapper

@bot.message_handler(commands=['start'])
def handle_start(message):
  state, text = parse_message(message)

  if state.style == 1:
    send_voice(state, "Я побрился")
  else:
    send_voice(state, "Я родился")
  send_voice(state, "/help для получания помощи")

@bot.message_handler(commands=['help'])
def handle_help(message):
  state, text = parse_message(message)

  if state.style == 0:
    send_voice(state, "Я могу озвучить любой (ну, почти) текст по команде /tts")
    send_voice(state, "Я могу рассказать анекдот по команде /anegdot")
    if state.kanegdotes:
      send_voice(state, "А ещё я иногда просто так рассказываю анекдоты, это можно отключить по команде /toggle_kanegdotes")
    else:
      send_voice(state, "А ещё я могу иногда просто так рассказывать анекдоты, но это нужно включить по команде /toggle_kanegdotes")
    send_voice(state, "Изменить голос можно командой /set_voice")
    send_voice(state, f"Я {'прикрепляю' if state.text else 'не прикрепляю'} к голосу текст, это поведение можно изменить по команде /toggle_text")
    send_voice(state, f"И ещё немного персонализации: если тебя бесит моя вежливость, /set_style изменит стиль сообщений")
  elif state.style == 1:
    send_voice(state, "Те нужна помощь?")
    send_voice(state, "Короч, пишешь /tts и свой текст который ты заставляешь меня озвучить")
    send_voice(state, "Или /anegdot чтобы у меня появилась свобода выбора")
    if state.kanegdotes:
      send_voice(state, "/toggle_kanegdotes выключит надоедливые ответы")
    else:
      send_voice(state, "/toggle_kanegdotes включит режим наблюдателя за ситуациями, когда мне захочется сказать анекдот просто так")
    send_voice(state, "После /set_voice я заговорь по-другому")
    send_voice(state, f"Я те вообщет текст к сообщениям {'прикрепляю' if state.text else 'не прикрепляю'} но /toggle_text это исправит")
    send_voice(state, f"И ещё немного персонализации: если тебя бесит моя грубость, /set_style изменит стиль сообщений")

@bot.message_handler(commands=['toggle_kanegdotes'])
@requires_licence
def handle_toggle_text(message):
  state, text = parse_message(message)
  state.kanegdotes = not state.kanegdotes
  save_chats()
  if state.kanegdotes:
    send_voice(state, "Теперь я буду иногда отвечать анекдотами")
  else:
    send_voice(state, "Теперь я больше не буду отвечать анекдотами")

@bot.message_handler(commands=['set_voice'])
def handle_set_voice(message):
  state, text = parse_message(message)

  try:
    voice = int(text.split()[1])
    assert(voice in range(len(rhvoice.voices)))
    if state.voice != voice:
      state.voice = voice
      save_chats()
      send_voice(state, "Голос изменён")
    else:
      send_voice(state, "Этот голос уже выбран")
  except:
    if state.style == 0: send_voice(state, f"Пожалуйста, укажите вторым 'аргументом' команды номер голоса (от 0 до {len(rhvoice.voices) - 1} включительно)")
    elif state.style == 1: send_voice(state, f"Иииии... Как мне по твоему сообщению понять как мне следует говорить?")

@bot.message_handler(commands=['toggle_text'])
def handle_toggle_text(message):
  state, text = parse_message(message)
  state.text = not state.text
  save_chats()
  if state.text:
    send_voice(state, "Теперь я буду прикреплять к аудиосообщениям текст")
  else:
    send_voice(state, "Теперь я больше не буду прикреплять к аудиосообщениям текст")

@bot.message_handler(commands=['request_licence'])
def handle_toggle_text(message):
  state, text = parse_message(message)
  save_chats()
  send_voice(chats["987521502"], f"{message.from_user.first_name} {message.from_user.last_name} запросил лицензию (id: {state.id})")

@bot.message_handler(commands=['set_style'])
def handle_set_style(message):
  state, text = parse_message(message)

  res = -1
  voice = " ".join(text.split()[1:]).lower()
  if voice == "вежливый": res = 0
  elif voice == "грубый": res = 1
  else:
    send_voice(state, "Все стили: 'вежливый', 'грубый'")
    return
  
  if state.style == res:
    send_voice(state, "Этот стиль уже выбран")
  else:
    state.style = res
    save_chats()
    send_voice(state, "Стиль изменён")

@bot.message_handler(commands=['tts'], content_types=['text'])
@requires_licence
def handle_tts(message):
  state, text = parse_message(message)
  send_voice(state, " ".join(text.split()[1:]))

@bot.message_handler(commands=['anegdot'])
@requires_licence
def handle_anegdot(message):
  state, text = parse_message(message)
  send_voice(state, anegdotes[random.randint(0, len(anegdotes) - 1)])

@bot.message_handler(content_types=['text'])
def handle_message(message):
  state, text = parse_message(message)
  if not state.has_licence: return
  
  for keyword in kanegdotes.keys():
    if keyword in text.lower():
      send_voice(state, kanegdotes[keyword][random.randint(0, len(kanegdotes[keyword]) - 1)])

def load_anegdotes():
  global anegdotes
  with open("anegdotes.txt", "r") as fs:
    anegdotes = fs.readlines()
    fs.close()

def load_all(*_):
  load_chats()
  load_anegdotes()

def save_all(*_):
  save_chats()

signal.signal(signal.SIGUSR1, load_all)
signal.signal(signal.SIGUSR2, save_all)

load_all()

# Запуск бота
bot.polling(none_stop=True)
